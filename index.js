//Express Setup

//1. 1. import by using the ' require directive to get access to the components of express package/ dependency
const { request, response } = require('express');
const express = require('express');
//2. use the express() function and assign it to an app variable to create an express app or app server
const app = express();
// 3. Declare a variable for the port of the server
const port = 3000;
// Middle wares
// These two .use are essential in express
// allows your app to res json format data
app.use(express.json());
//allows your app to read data from forms
app.use(express.urlencoded({extended: true}));

// Routes

//4. Get request route
app.get('/', (req, res) => {
    // once the route is accessed it will then send a string response containing "Hello World"
    res.send('Hello World!')
})

app.get('/hello', (req, res) => {
    res.send('Hello from /hello endpoint!')
})

// Register user route
/*An array that will store user objects/ documents when the "/register" route is accessed
>This will also serve as our mock database */

let users = [];
/*This route expects to receive a POST request at the URI '/register 
> This will create a suer object in the "users" variable that mirrors a real world registration process*/
app.post('/register', (req, res) => {
    if( req.body.username !== "" && req.body.password !== "") {
        users.push(req.body)
        console.log(users)
        res.send(`User ${req.body.username} successfully registered`)
    }else{
        res.send(`Incorrect input username and password`)
    }
})
// PUT request route
/*This route expects to receive a PUT request at the URI "/change-password
> this will update the password of a user that matches the information provided in the client/ Postman (request.body) */
app.put('/change-password', (req, res) => {
    let message
    // declare a message variable to store the message to be sent back to the client
    for(let i = 0; i < users.length; i++){
        //if a username matches, reassign / update the existing user's password
        if(req.body.username == users[i].username){
            users[i].password == req.body.password
            message = `User ${req.body.username}'s password has been updated!`
            break;
        }else{
            // if no user matches, set the message var to a string saying the user does not exist
            message = `User does not exist.`
        }
    } // send a responde back to the client/ postman once the password has been updated or if the user is not found
    res.send(message)
})


// ACTIVITY
//GET
app.get('/home', (req, res) => {
    res.send('Welcome to the user home page!')
})

//GET Users
app.get('/users', (req, res) => {
    res.send(users)
})


//DELETE
app.delete('/delete-user', (req,res)=>{
    let message

	for(let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username){
			users.splice(i,1);
			message = `User ${req.body.username} has been deleted!`
			break
		} else {
			message = 'User does not exist.'
		}
	}
	res.send(message)
})


// use to check if the server is running
app.listen(port, () => console.log(`Server is running at port ${port}`))

